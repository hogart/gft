'use strict';

const Model = require('./Model');

class ListModel extends Model {
    constructor (options) {
        super(options);

        this.storage = options.storage;

        this.load();

        this.on('change', this.save, this);
    }

    addItem (item) {
        this._data.list.push(item);
        this.fire('change');
    }

    removeItem (index) {
        this._data.list.splice(index, 1);
        this.fire('change');
    }

    save () {
        this.storage.setItem('list', JSON.stringify(this._data.list));
    }

    load () {
        let saved = this.storage.getItem('list');
        if (saved) {
            try {
                saved = JSON.parse(saved);
            } catch (e) {
                saved = [];
                console.error(e);
            }
        } else {
            saved = [];
        }

        this.set({list: saved});
    }
}

module.exports = ListModel;