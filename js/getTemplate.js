'use strict';

const fnd = require('fnd');
const _ = require('underscore');
const templates = {};

function getTemplate (tplName) {
    if (!(tplName in templates)) {
        const tplNode = fnd('script[type="text/template"].js-tpl-' + tplName);
        if (!tplNode) {
            throw new Error('No such template: ' + tplName);
        } else {
            templates[tplName] = _.template(tplNode[0].textContent.trim());
        }
    }

    return templates[tplName];
}

module.exports = getTemplate;