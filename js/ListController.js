'use strict';

const Controller = require('./Controller');

class ListController extends Controller {
    constructor (options) {
        super(options);

        this.onModelChange();
    }

    events () {
        return {
            'click .js-del': 'removeItem'
        }
    }

    removeItem (event) {
        let index = event.target.parentNode.dataset.index;
        index = parseInt(index, 10);

        this.model.removeItem(index);
    }
}

module.exports = ListController;