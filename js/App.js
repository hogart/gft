'use strict';

const Observer = require('./Observer');

class App extends Observer {
    constructor (root, config, fnd) {
        super();

        this.root = root;
        this.config = config;

        this.fnd = fnd;
    }

    $ (selector) {
        return this.fnd(selector, this.root);
    }

    init () {
        const controllers = {};
        Object.keys(this.config).forEach(function (selector) {
            const conf = this.config[selector];
            const el = this.$(selector);

            if (!el) {
                throw new Error('Missing el: ' + selector)
            }

            const model = conf.model instanceof Function ? new conf.model() : conf.model;
            const view = conf.view instanceof Function ? new conf.view({el}) : conf.view;
            const controller = new conf.controller({view, model});

            controllers[selector] = controller;
        }, this);

        this.controllers = controllers;
    }
}

module.exports = App;