'use strict';

const Observer = require('./Observer');

class Model extends Observer {
    constructor (options) {
        options = (options || {});
        super();

        this._data = options.data || {};
    }

    set (values) {
        const keys = Object.keys(values);

        keys.forEach(function (key) {
            this._data[key] = values[key];
        }, this);

        this.fire('change');
    }

    get (key) {
        return this._data[key] || undefined;
    }

    has (key) {
        return (key in this._data);
    }

    delete (key) {
        delete this._data[key];
        this.fire('change');
    }
}

module.exports = Model;