'use strict';

class Observer {
    constructor () {
        this._events = {}
    }

    on (eventName, handler, ctx) {
        if (!this._events[eventName]) {
            this._events[eventName] = [];
        }

        const handlerData = ctx ? {fn: handler, ctx} : handler;

        this._events[eventName].push(handlerData);
    }

    off (eventName, handler, ctx) {
        const handlers = this._events[eventName];

        if (!handler) {
            handlers.length = 0;
        } else {
            if (ctx) {
                for (let i = handlers.length - 1; i >= 0; i--) { // reverse cycle is faster and allows array modification in process
                    if (ctx === handlers[i].ctx || handler === handlers[i].fn) {
                        handlers.splice(i, 1);
                    }
                }
            } else {
                for (let i = handlers.length - 1; i >= 0; i--) { // reverse cycle is faster and allows array modification in process
                    if (handler === handlers[i] || handler === handlers[i].fn) {
                        handlers.splice(i, 1);
                    }
                }
            }
        }
    }

    fire (eventName) {
        const handlers = this._events[eventName];
        if (!handlers) {
            return;
        }

        const params = [].slice.call(arguments, 1);

        handlers.forEach(function (handler) {
            if (handler.ctx) {
                handler.fn.apply(handler.ctx, params);
            } else {
                handler.apply(null, params);
            }
        });
    }

    /**
     * Gracefully shut off, unbinding all listeners
     * @destructor
     */
    destroy () {
        Object.keys(this._events).forEach(function (eventName) {
            this._events[eventName].length = 0;
        });
    }
}

module.exports = Observer;