'use strict';

const Observer = require('./Observer');
const fnd = require('fnd');
const evt = require('fnd/fnd.evt');
const _ = require('underscore');

class Controller extends Observer {
    constructor (options) {
        super();

        this.view = options.view;
        this.model = options.model;

        this.model.on('change', this.onModelChange, this);

        this.listenDom();
    }

    listenDom () {
        if (this._unlistenDom) {
            this._unlistenDom();
        }

        this._unlistenDom = evt(this.view.el, _.result(this, 'events') || {}, this);
    }

    onModelChange () {
        const data = this.model._data;
        this.view.render(data);
    }

    destroy () {
        this.model.off('change', this.onModelChange, this);

        if (this._unlistenDom) {
            this._unlistenDom();
        }

        super.destroy();
    }
}

module.exports = Controller;