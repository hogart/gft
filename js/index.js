'use strict';

const fnd = require('fnd');
const ListModel = require('./ListModel');
const App = require('./App');

const listModel = new ListModel({storage: localStorage});

const appConf = {
    '.js-newItem': {
        model: listModel,
        view: require('./AddItemView'),
        controller: require('./AddItemController')
    },

    '.js-list': {
        model: listModel,
        view: require('./ListView'),
        controller: require('./ListController')
    },

    '.js-counter': {
        model: listModel,
        view: require('./CounterView'),
        controller: require('./CounterController')
    }
};

const appRoot = fnd('.js-app');

const app = new App(appRoot, appConf, fnd);
app.init();

