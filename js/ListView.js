'use strict';

const View = require('./View');

class ListView extends View {
    tpl () {
        return 'list'
    }
}

module.exports = ListView;