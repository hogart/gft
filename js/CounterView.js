'use strict';

const View = require('./View');

class CounterView extends View {
    tpl () {
        return 'counter'
    }
}

module.exports = CounterView;