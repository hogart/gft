'use strict';

const Controller = require('./Controller');

class AddItemController extends Controller {
    constructor (options) {
        super(options);

        this.onModelChange();
    }

    events () {
        return {
            'click .js-addBtn': 'addItem'
        }
    }

    addItem () {
        const item = this.view.input.value.trim();

        if (item) {
            this.model.addItem(item);
        }
    }
}

module.exports = AddItemController;