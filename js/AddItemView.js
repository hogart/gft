'use strict';

const View = require('./View');
const fnd = require('fnd');

class AddItemView extends View {
    tpl () {
        return 'addItem'
    }

    render (data) {
        super.render(data);

        this.input = fnd('input[type="text"]', this.el)[0];
    }
}

module.exports = AddItemView;