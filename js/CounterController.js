'use strict';

const Controller = require('./Controller');

class CounterController extends Controller {
    constructor (options) {
        super(options);

        this.onModelChange();
    }
}

module.exports = CounterController;