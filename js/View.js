'use strict';

const Observer = require('./Observer');
const _ = require('underscore');
const getTemplate = require('./getTemplate');

class View extends Observer {
    constructor (options) {
        options = (options || {});
        super();

        this.el = options.el[0];
        this.template = getTemplate(_.result(this, 'tpl'));
    }

    render (data) {
        this.el.innerHTML = this.template(data);
    }
}

module.exports = View;