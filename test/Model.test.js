'use strict';

const mocha = require('mocha'); // lol
const assert = require('chai').assert;
const describe = mocha.describe;
const it = mocha.it;

const Model = require('../js/Model');

describe('Model', function () {
    it('sets and gets data by key', function () {
        const md = new Model();

        md.set({answer: 42});
        assert.equal(md.get('answer'), 42);

        md.set({key1: 1, key2: 2});
        assert.equal(md.get('key1'), 1);
        assert.equal(md.get('key2'), 2);

        md.set({answer: 43});
        assert.equal(md.get('answer'), 43);
    });

    it('deletes values by key', function () {
        const md = new Model();

        md.set({answer: 42});
        assert.equal(md.get('answer'), 42);

        md.delete('answer');
        assert.isUndefined(md.get('answer'));
    });

    it('fires change event', function () {
        let counter = 0;
        const md = new Model();

        md.on('change', function () {
            counter++;
        });

        md.set({answer: 42});
        md.set({answer: 43});

        md.delete('answer');

        assert.equal(counter, 3);
    });
});