'use strict';

const mocha = require('mocha');
const assert = require('chai').assert;
const describe = mocha.describe;
const it = mocha.it;

const App = require('../js/App');

describe('App', function () {
    it('creates instances of models, views and controllers', function () {
        let counter = 0;
        const conf = {
            'selector': {
                controller: function () { counter++; },
                model: function () { counter++; },
                view: function () { counter++; }
            }
        };
        const ap = new App(null, conf);
        ap.$ = function () {
            return arguments[0];
        };

        ap.init();
    })
});