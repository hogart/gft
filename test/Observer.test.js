'use strict';

const mocha = require('mocha');
const assert = require('chai').assert;
const describe = mocha.describe;
const it = mocha.it;

const Observer = require('../js/Observer.js');

describe('Observer', function () {
    it('listens and fires events', function (done) {
        const ob = new Observer();

        ob.on('someEvent', function () {
            assert.ok('event fired');
            done();
        });

        ob.fire('someEvent');
    });

    it('correctly passes event arguments to handlers', function (done) {
        const ob = new Observer();

        ob.on('someEvent', function (param1, param2) {
            assert.equal(param1, 123);
            assert.equal(param2, '456');
            done();
        });

        ob.fire('someEvent', 123, '456');
    });

    it('allows handlers to have context', function (done) {
        const ob = new Observer();

        const listener = {
            callback (param1, param2) {
                assert.equal(this, listener, 'correct context');
                assert.equal(param1, 123);
                assert.equal(param2, '456');
                done();
            }
        }

        ob.on('someEvent', listener.callback, listener);
        ob.fire('someEvent', 123, '456');
    });

    it('deletes handlers without context', function () {
        const ob = new Observer();
        let counter = 0;
        function handler () {
            counter++;
        };

        ob.on('someEvent', handler);
        ob.fire('someEvent');
        ob.off('someEvent', handler);
        ob.fire('someEvent');

        assert.equal(counter, 1, 'handler called only once');
    });

    it('deletes handlers with context', function () {
        const ob = new Observer();
        let counter = 0;
        const listener = {
            callback () {
                counter++;
            }
        }

        ob.on('someEvent', listener.callback, listener);
        ob.fire('someEvent');
        ob.off('someEvent', listener.callback, listener);
        ob.fire('someEvent');

        assert.equal(counter, 1, 'handler called only once');
    });
});