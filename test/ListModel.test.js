'use strict';

const mocha = require('mocha');
const assert = require('chai').assert;
const describe = mocha.describe;
const it = mocha.it;

const ListModel = require('../js/ListModel');
const storage = {
    item: undefined,

    getItem () {
        return this.item;
    },

    setItem (key, val) {
        this.item = val;
    }
}

describe('ListModel', function () {
    beforeEach(function () {
        storage.item = undefined;
    });

    it('fires change events when adding items to list', function () {
        const lm = new ListModel({storage});
        let counter = 0;

        lm.on('change', function () {
            counter++;
        });

        lm.addItem('123');
        lm.addItem('456');
        lm.addItem('789');
        lm.removeItem(0);
        lm.removeItem(1);

        assert.equal(counter, 5);
    });

    it('correctly handles item adding/removing', function () {
        const lm = new ListModel({storage});

        lm.addItem('123');
        lm.addItem('456');

        assert.lengthOf(lm.get('list'), 2);

        lm.addItem('789');
        lm.removeItem(0);

        assert.deepEqual(lm.get('list'), ['456', '789']);

        lm.removeItem(1);

        assert.deepEqual(lm.get('list'), ['456']);
    });
});