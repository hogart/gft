Architecture
============

App utilizes text-book MVC with separate models, views and controllers. As per definition, model provides data, view's
responsibility is to show UI, and controller glues first two together and handles user input. To provide extensibility
and code reuse, all entities implemented as classes, inherited from Observer.

DOM events are described in controllers in a way very similar to Backbone.js, because underlying library supports it
directly.

App itself is little more than POJO config, binding DOM selectors with appropriate controllers, views and models. In
this config model can be shared between several controllers.

App uses ES2015 dialect currently available in browsers (Chrome 44).

Libraries
=========

Native DOM is messy and verbose. To avoid typing `.querySelectorAll` and `.addEventListener` countless times, app uses
[fnd](https://github.com/hogart/fnd), performant micro-library for DOM traversal and even handling, written by me.

Tests are written using mocha and assert.js. These two libraries were choosen for clear separation of concerns.

Underscore is used -- mostly for it's small and robust templating solution.



