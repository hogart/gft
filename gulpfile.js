'use strict';

const gulp = require('gulp');
const stylus = require('gulp-stylus');
const nib = require('nib');
const browserify = require('browserify');
const sourcemaps = require('gulp-sourcemaps');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

gulp.task('styles', function () {
    gulp.src('./css/*.styl')
        .pipe(sourcemaps.init())
        .pipe(stylus({use: nib(), 'include css': true}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./'));
});

function scriptTask (entryPoint) {
    const b = browserify({
        entries: `./js/${entryPoint}.js`,
        debug: true
    });

    const bundled = b.bundle()
        .pipe(source(`${entryPoint}.js`))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}));

    return bundled
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./'));
}

gulp.task('js', function () {
    return scriptTask('index');
});

gulp.task('watch', function () {
    gulp.watch('./js/**/*.js', ['js']);
    gulp.watch('./css/**/*.styl', ['styles']);
});

gulp.task('default', ['js', 'styles', 'watch']);